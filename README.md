# README #

This is a code repository for a blogging web application built with:

* ASP.NET MVC
* NHibernate
* Ninject
* SQL Server
* LINQ
* Javascript, css, HTML5 and etc

The Web Application was started following a [tutorial by Vijaya Anand](http://www.prideparrot.com/blog/archive/2012/12/how_to_create_a_simple_blog_part1) but I do have plans to take it further.

### Have a question? ###

Get in touch with [Sir Hoe](http://sirhoe.github.io)