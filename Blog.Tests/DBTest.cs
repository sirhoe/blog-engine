﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Blog.Core;
using System.Data.SqlServerCe;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Blog.Core.Objects;

namespace Blog.Tests
{
    [TestClass]
    public class DBTest
    {
        [TestMethod]
        public void CanConnectDb()
        {
            SqlCeConnection conn = null;

            try
            {
                conn = new SqlCeConnection("Data Source = Blog.sdf");
                conn.Open();
            }
            finally
            {
                conn.Close();
            }
        }

        [TestMethod]
        public void FluentWithSqlCompact()
        {
            var fac = Fluently.Configure()
                 .Database(MsSqlCeConfiguration.Standard.ConnectionString("Data Source=Blog.sdf"))
                 .Mappings(m =>
                 {
                     m.FluentMappings.AddFromAssembly(typeof(Post).Assembly);
                 })
                 .BuildConfiguration()
                 .BuildSessionFactory();
        }
    }
}
