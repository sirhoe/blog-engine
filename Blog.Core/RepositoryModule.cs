﻿using NHibernate;
using NHibernate.Cache;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using Blog.Core.Objects;
using NHibernate.Tool.hbm2ddl;


namespace Blog.Core
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ISessionFactory>()
                .ToMethod
                (
                    e =>
                        Fluently.Configure()
                       
                        .Database(MsSqlConfiguration.MsSql2012.ConnectionString(c =>               
                            c.FromConnectionStringWithKey("BlogDBConnectionString")))
                        .Cache(c => c.UseQueryCache().ProviderClass<HashtableCacheProvider>())
                        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<Post>())
                        //Recreates the db everytime
                        //.ExposeConfiguration(cfg => new SchemaExport(cfg).Execute(true, true, false))
                        .BuildConfiguration()
                        .BuildSessionFactory()
                )
                .InSingletonScope();

            Bind<ISession>()
                .ToMethod(
                (ctx) => ctx.Kernel.Get<ISessionFactory>().OpenSession())
                .InRequestScope();
            
        }

    }
}
