﻿using Blog.Core.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Core
{
    public interface IBlogRepository
    {
        IList<Post> Posts(int pageNo, int pageSize);
        int TotalPost();
        IList<Post> PostForCategory(string categorySlug, int pageNo, int pageSize);
        int TotalPostForCategory(string categorySlug);
        Category Category(string categorySlug);
        IList<Post> PostForTag(string tagSlug, int pageNo, int pageSize);
        int TotalPostForTag(string tagSlug);
        Tag Tag(string tagSlug);
        IList<Post> PostForSearch(string search, int pageNo, int pageSize);
        int TotalPostForSearch(string seach);
        Post Post(int month, int year, string titleSlug);
        IList<Category> Categories();
        IList<Tag> Tags();
    }
}
