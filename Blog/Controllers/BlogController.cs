﻿using Blog.Core;
using Blog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Blog.Controllers
{
    public class BlogController : Controller
    {
        private readonly IBlogRepository _blogRepo;

        public BlogController(IBlogRepository blogRepo)
        { _blogRepo = blogRepo; }

        public ViewResult Posts(int p = 1)
        {
            var viewModel = new ListViewModel(_blogRepo, p);

            ViewBag.Title = "Latest Post";
            return View("List", viewModel);
        }

        public ViewResult Category(string category, int p = 1)
        {
            var viewModel = new ListViewModel(_blogRepo, p, category, "Category");

            if (viewModel.Category == null)
                throw new HttpException(404, "Category Not Found");

            ViewBag.Title = String.Format(@"Latest Post on Category ""{0}""", viewModel.Category.Name);
            return View("List", viewModel);
        }

        public ViewResult Tag(string tag, int p = 1)
        {
            var viewModel = new ListViewModel(_blogRepo, p, tag, "Tag");

            if (viewModel.Tag == null)
                throw new HttpException(404, "Tag Not Found");
        
            ViewBag.Title = String.Format(@"Latest Post on Tag ""{0}""", viewModel.Tag.Name);

            return View("List", viewModel);
        }

        public ViewResult Search(string s, int p = 1)
        {
            var viewModel = new ListViewModel(_blogRepo, p, s, "Search");
            ViewBag.Title = String.Format(@"List of post found
                for search text ""{0}""", s);
            return View("List", viewModel);

        }

        public ViewResult Post(int year, int month, string title)
        {
            var post = _blogRepo.Post(year, month, title);

            if (post == null)
                throw new HttpException(404, "Post not found");
            if (!post.Published && !User.Identity.IsAuthenticated)
                throw new HttpException(401, "Post not published");

            return View(post);
        }

        [ChildActionOnly]
        public PartialViewResult Sidebars()
        {
            var widget = new WidgetViewModel(_blogRepo);
            return PartialView("_Sidebars", widget);
        }
            
            
        public ActionResult Index()
        {
            return View();
        }

    }
}
