﻿using Blog.Core;
using Blog.Core.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models
{
    public class ListViewModel
    {
        /// <summary>
        /// Contrusctor
        /// </summary>
        /// <param name="blogRepo">DB</param>
        /// <param name="p">page number</param>
        public ListViewModel(IBlogRepository blogRepo, int p)
        {
            Posts = blogRepo.Posts(p - 1, 10);
            TotalPosts = blogRepo.TotalPost();
        }

        public ListViewModel(IBlogRepository blogRepo, int p
            , string text, string type)
        {
            switch(type)
            {
                case "Category":
                    Posts = blogRepo.PostForCategory(text, p - 1, 10);
                    TotalPosts = blogRepo.TotalPostForCategory(text);
                    Category = blogRepo.Category(text);
                    break;
                case "Tag":
                    Posts = blogRepo.PostForTag(text, p-1, 10);
                    TotalPosts = blogRepo.TotalPostForTag(text);
                    Tag = blogRepo.Tag(text);
                    break;
                default:
                    Posts = blogRepo.PostForSearch(text, p - 1, 10);
                    TotalPosts = blogRepo.TotalPostForSearch(text);
                    Search = text;
                    break;
            }
        }

        public IList<Post> Posts { get;  private set; }
        public int TotalPosts { get;  private set; }
        public Category Category { get; set; }
        public Tag Tag { get; set; }
        public string Search { get; set; }
    }
}